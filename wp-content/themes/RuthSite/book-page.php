<?php
/*
Template Name: Book Page
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php
	$breadcrumbs = ( function_exists('get_breadcrumb') ) ? get_breadcrumb() : null;

	$book_subtitle = get_field('book_subtitle');
	if (strlen(trim($book_subtitle)) > 0)
		$book_subtitle = ': ' . $book_subtitle;

	$all_book_series = get_all_book_series();
	$book_series_key = get_field('book_series');

	$book_cover = get_field('book_cover');
	$book_excerpt = get_field('book_excerpt');

	$awards = get_field('awards');
	$read_an_excerpt = get_field('read_an_excerpt');
	$teachers_guide = get_field('teachers_guide');
	$reviews = get_field('reviews');

	$related_books_args = array(
		'post_type' => 'page',
		'posts_per_page' => -1,
		'post__not_in' => array(get_the_ID()), // Exclude the current book.
		'meta_key' => 'book_series',
		'meta_value' => get_field('book_series'),
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$related_books = new WP_Query( $related_books_args );
?>

<h1 class="page-title">Books</h1>

<div class="container">
	<?php if ( !empty($breadcrumbs) ): ?>
	<div class="row">
		<div class="col-12">
			<?php echo $breadcrumbs; ?>
		</div>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-12 content-background-no-pad holder">
			<div class="row">
				<div class="col-12 col-sm-5 col-lg-4 action-sidebar even-height">
					<img class="action-image" src="<?php echo $book_cover['url']; ?>" alt="<?php echo $book_cover['alt']; ?>" title="<?php echo $book_cover['title']; ?>"/>

					<?php book_buy_buttons(); ?>

					<?php if (strlen(trim($awards)) > 0): ?>
					<div class="book-awards">
					<?php the_field('awards'); ?>
					</div>
					<?php endif; ?>

					<?php if ( $related_books->have_posts() ): ?>
					<div class="related-info">
						<h4>Related Books</h4>
						<ul>
							<?php while ( $related_books->have_posts() ): $related_books->the_post(); ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; ?>
							<?php wp_reset_query(); /* REQUIRED */ ?>
						</ul>
					</div>
					<?php endif; ?>

				</div>
				<div class="col-12 col-sm-7 col-lg-8 even-height">
					<div class="main-info">
						<h3><?php the_title(); ?><?php echo $book_subtitle; ?></h3>
						<?php if ( ! empty( $book_series_key ) ): ?>
						<h5 class="book-series-title"><?php echo $all_book_series[$book_series_key]; ?></h5>
						<?php endif; ?>
						<?php the_content(); ?>

					<?php if (is_array($read_an_excerpt) && strlen(trim($read_an_excerpt['url'])) > 0): ?>
					<a class="book-content-btn btn btn-primary" href="<?php echo $read_an_excerpt['url']; ?>" alt="<?php echo $read_an_excerpt['alt']; ?>" title="<?php echo $read_an_excerpt['title']; ?>"/>Read an Excerpt</a>
					<?php endif; ?>

					<?php if (strlen(trim($teachers_guide)) > 0): ?>
					<a class="book-content-btn btn btn-primary" href="<?php echo $teachers_guide; ?>">Teacher's Guide</a>
					<?php endif; ?>


					<?php if (strlen(trim($reviews)) > 0): ?>
						<h4>A Few Reviews</h4>
						<?php the_field('reviews'); ?>
					<?php endif; ?>
					</div>

				</div>
			</div><!--close .row-->
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->

<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>

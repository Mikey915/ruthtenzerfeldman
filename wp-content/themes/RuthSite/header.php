<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic|Alegreya:400italic,700italic,400,700|Alegreya+SC:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
<link rel="stylesheet" href="/wp-content/themes/RuthSite/ruth.css" type="text/css" media="all">
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>

<div id="wrapper">
<header id="masthead" class="site-header" role="banner">
	<div class="container">
		<div class="row">
			<div class="site-header-inner col-12 col-lg-offset-1">
					
				<?php $header_image = get_header_image();
				if ( ! empty( $header_image ) ) { ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						 <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt=""> 
					</a>
				<?php } // end if ( ! empty( $header_image ) ) ?> 
				
					<div class="site-branding">
						<!-- <img alt="ruth tenzer feldman wearing a pink blouse" src="/wp-content/themes/RuthSite/ruth-tenzer-feldman.jpg">-->
						<div class="site-image"></div>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>
					</div>
						
			</div>
		</div>
	</div><!-- .container -->
	<nav class="site-navigation">		
	<div class="container">
		<div class="row">
			<div class="site-navigation-inner col-12">
				<div class="navbar">
					<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>
				
					<!-- Your site title as branding in the menu -->
					<!--<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>-->
					
					<!-- The WordPress Menu goes here -->
					<div class="non-semantic-protector">
					   <?php wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container_class' => 'nav-collapse collapse navbar-responsive-collapse nav-ribbon',
								'menu_class' => 'nav navbar-nav',
								'fallback_cb' => '',
								'menu_id' => 'main-menu',
								'walker' => new wp_bootstrap_navwalker()
							)
						); ?>
					</div>
				
				</div><!-- .navbar -->
			</div>
		</div>
	</div><!-- .container -->
</nav><!-- .site-navigation -->
</header><!-- #masthead -->
		


<div class="main-content">

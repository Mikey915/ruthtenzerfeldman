<?php
/*
Template Name: About Page
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php
	$about_page_title = 'About';

	$breadcrumbs = ( function_exists('get_breadcrumb') ) ? get_breadcrumb() : null;

	$author_photo = get_field('author_photo');

	$about_page = get_page_by_title( $about_page_title );
	$current_page = get_permalink();

	$subpages_args = array(
		'post_parent' => $about_page->ID,
		'post_type' => 'page',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$subpages = new WP_Query( $subpages_args );

	$latest_book = get_latest_book();
?>

<h1 class="page-title">About</h1>

<div class="container">
	<?php if ( !empty($breadcrumbs) ): ?>
	<div class="row">
		<div class="col-12">
			<?php echo $breadcrumbs; ?>
		</div>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-12 content-background-no-pad holder">
			<div class="row">
				<div class="col-12 col-sm-5 col-lg-4 action-sidebar even-height">
					<img class="action-image" src="<?php echo $author_photo['url']; ?>" alt="<?php echo $author_photo['alt']; ?>" title="<?php echo $author_photo['title']; ?>"/>
					<?php if ( $subpages->have_posts() ): ?>
					<div class="related-info">
						<h3>More About Ruth</h3>
						<ul>
							<?php if ( get_the_title() !== $about_page_title ): ?>
							<li><a href="<?php echo '/' . $about_page->post_name; ?>">Bio</a></li>
							<?php endif; ?>
							<?php while ( $subpages->have_posts() ): $subpages->the_post(); ?>
							<?php if ( $current_page !== get_permalink() ): ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php wp_reset_query(); /* REQUIRED */ ?>
						</ul>
					</div>
					<?php endif; ?>
					<?php
						if ( $latest_book->have_posts() ): $latest_book->the_post();
							$book_cover = get_field('book_cover');
					?>
					<div class="book-widget">
						<h3>My Latest Book</h3>
						<img alt="<?php echo $book_cover['alt']; ?>" src="<?php echo $book_cover['url']; ?>" title="<?php echo $book_cover['title']; ?>"/>
						<h4><?php the_title(); ?></h4>
						<?php the_field('short_description'); ?>
						<a class="learn-more-link" href="<?php the_permalink(); ?>">Learn more <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></a>
						<?php book_buy_button_dropdown(); ?>
					</div>
					<?php endif; ?>
					<?php wp_reset_query(); /* REQUIRED */ ?>
				</div>
				<div class="col-12 col-sm-7 col-lg-8 even-height">
					<div class="main-info">
						<h3><?php the_title(); ?></h3>
						<?php the_content(); ?>
					</div>

				</div>
			</div><!--close .row-->
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->

<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>

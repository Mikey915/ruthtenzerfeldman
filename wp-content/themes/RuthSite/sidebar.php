<?php
/**
 * The sidebar containing the main widget area
 *
 * @package _tk
 */
?>

	</div><!-- close .main-content-inner -->

	<div class="sidebar col-12 col-lg-3">

		<?php // add the class "panel" below here to wrap the sidebar in Bootstrap style ;) ?>
		<div class="sidebar-padder">

			<?php if ( is_active_sidebar( 'sidebar-2') ) : ?>
			<aside id="inspiration">
				<?php do_action( 'before_sidebar' ); ?>
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</aside>
			<?php endif; ?>

			<?php if ( is_active_sidebar( 'sidebar-3') ) : ?>
			<aside id="mailing-list-signup">
				<?php do_action( 'before_sidebar' ); ?>
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</aside>
			<?php endif; ?>

            <!--
			<ul id="social-media-icons">
				<li class="facebook"><a href="https://www.facebook.com/ruth.t.feldman"></a></li>
				<li class="twitter"><a href="https://twitter.com/ScrivaRuth"></a></li>
				<li class="goodreads"><a href="http://www.goodreads.com/author/show/600253.Ruth_Tenzer_Feldman"></a></li>
				<li class="rss"><a href="http://ruthdev.kateburkett.com/feed"></a></li>
			</ul>

			<aside id="main-sidebar">
				<?php
					$blue_thread_args = array(
						'page_id' => 3361, // Blue Thread
						'post_type' => 'page',

						// These should be unnecessary, but better safe than sorry.
						'posts_per_page' => 1,
						'orderby' => 'date',
						'order' => 'ASC'
					);
					$blue_thread = new WP_Query( $blue_thread_args );

					if ( $blue_thread->have_posts() ) : $blue_thread->the_post();
						$book_cover = get_field('book_cover');
				?>
				<h3>Get the Oregon Book Award Winner</h3>
				<img alt="<?php echo $book_cover['alt']; ?>" src="<?php echo $book_cover['url']; ?>" title="<?php echo $book_cover['title']; ?>"/>
				<?php the_field('short_description'); ?>
				<?php book_buy_button_dropdown(); ?>
				<p>
					<a href="<?php the_permalink(); ?>" class="btn btn-default btn-lg">Learn more</a>
				</p>
				<?php endif; ?>
				<?php wp_reset_query(); /* REQUIRED */ ?>
				<h3>Need a speaker?</h3>
				<img alt="ruth tenzer feldman wearing a feathered hat and a Votes For Women sash" src="/wp-content/themes/RuthSite/img/ruth-vfw-speaking.jpg">
				<p>Interested in classroom visits? Writing workshops? Book club meetings? Formal lectures? Cyber-chats? I've had a variety of experience, from high school English classes, to mother-daughter book groups, and I'm happy to help you with your next event.</p>
				<a href="/speaking" class="pull-right">Learn more <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></a>
				<div class="clearfix"></div>
			</aside>
            -->

			<?php if ( is_active_sidebar( 'sidebar-1') ) : ?>
			<aside id="blog-sidebar">
				<?php do_action( 'before_sidebar' ); ?>
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</aside>
			<?php endif; ?>

		</div><!-- close .sidebar-padder -->

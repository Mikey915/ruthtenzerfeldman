<?php
/*
Template Name: Guides & Resources Page
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>

<h1 class="page-title">Explore!</h1>
<?php
	$current_page = get_the_ID();
	$subnav_args = array(
		'post_parent' => $post->post_parent, // Get the subs of the Explore page
		'post_type' => 'page',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$subnav = new WP_Query( $subnav_args );

	$resources_args = array(
		'post_parent' => $current_page, // Get the subs of the current page
		'post_type' => 'page',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$resources = new WP_Query( $resources_args );
?>

<div class="container">
	<div class="row">
		<div class="main-content-inner col-12">
			<div class="col-12 col-lg-3">
				<nav class="subpage-nav">
					<ul class="non-semantic-protector">
					<?php
						while ( $subnav->have_posts() ) : $subnav->the_post();
							$class_str = ( get_the_ID() == $current_page ) ? ' class="active"' : '';
					?>
						<li<?php echo $class_str; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; // end subnav loop. ?>
					<?php wp_reset_query(); /* REQUIRED */ ?>
					</ul>
				</nav>
			</div><!--end col-lg-3-->
			<div class="col-12 col-lg-9">
				<div class="content-background-half-pad">
					<h3><?php the_title(); ?></h3>
					<?php the_content(); ?>

					<ul class="resource">
					<?php
						while ( $resources->have_posts() ) : $resources->the_post();
							$resource_picture = get_field('resource_picture');
					?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<div class="resource-img-wrapper">
									<img class="resource-img" src="<?php echo $resource_picture['url']; ?>" alt="<?php echo $resource_picture['alt']; ?>" title="<?php echo $resource_picture['title']; ?>"/>
								</div>
								<div class="resource-data">
									<h4><?php the_title(); ?></h4>
									<p><?php the_field('resource_description'); ?></p>
								</div>
							</a>
						</li>
					<?php endwhile; // end resources loop. ?>
					<?php wp_reset_query(); /* REQUIRED */ ?>
					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->

<?php endif; ?>

<?php get_footer(); ?>

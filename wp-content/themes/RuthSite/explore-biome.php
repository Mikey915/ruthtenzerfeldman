<?php
/*
Template Name: Biome Earth Page
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>

<h1 class="page-title" id="top">Explore!</h1>
<?php
	$current_page = get_the_ID();
	$subnav_args = array(
		'post_parent' => $post->post_parent, // Get the subs of the Explore page
		'post_type' => 'page',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$subnav = new WP_Query( $subnav_args );

	$biome_posts_args = array(
		'category_name' => 'biome-earth',
		'post_type' => 'post',
		'posts_per_page' => -1
	);
	$biome_posts = new WP_Query( $biome_posts_args );
?>

<div class="container">
	<div class="row">
		<div class="main-content-inner col-12">
			<div class="col-12 col-lg-3">
				<nav class="subpage-nav">
					<ul class="non-semantic-protector">
					<?php
						while ( $subnav->have_posts() ) : $subnav->the_post();
							$class_str = ( get_the_ID() == $current_page ) ? ' class="active"' : '';
					?>
						<li<?php echo $class_str; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; // end subnav loop. ?>
					<?php wp_reset_query(); /* REQUIRED */ ?>
					</ul>
				</nav>
			</div><!--end col-lg-3-->
			<div class="col-12 col-lg-9">
				<div class="content-background-half-pad">
					<h3><?php the_title(); ?></h3>
					<?php the_content(); ?>
					<ul id="biome-toc">
						<li><a href="#post-3793">Antarctic Icefish</a></li>
						<li><a href="#post-3801">Aspilia</a></li>
						<li><a href="#post-3750">Bald Eagle</a></li>
						<li><a href="#post-3732">Beaver</a></li>
						<li><a href="#post-3787">Black Bear</a></li>
						<li><a href="#post-3754">Black Fly</a></li>
						<li><a href="#post-3791">Cacao</a></li>
						<li><a href="#post-3706">Caribou</a></li>
						<li><a href="#post-3748">Carob</a></li>
						<li><a href="#post-3710">Cockroach</a></li>
						<li><a href="#post-3708">Cranberry</a></li>
						<li><a href="#post-3726">Crane</a></li>
						<li><a href="#post-3803">Damselfly</a></li>
						<li><a href="#post-3795">Deep-sea Sponge</a></li>
						<li><a href="#post-3797">Dingo</a></li>
						<li><a href="#post-3734">Elm</a></li>
						<li><a href="#post-3764">Fire Ant</a></li>
						<li><a href="#post-3799">Fire-seeking Beetle</a></li>
						<li><a href="#post-3783">Four-leaf Clover</a></li>
						<li><a href="#post-3704">Fox</a></li>
						<li><a href="#post-3772">Great White Shark</a></li>
						<li><a href="#post-3720">Green Bottle Fly</a></li>
						<li><a href="#post-3702">Grouse</a></li>
						<li><a href="#post-3756">Hagfish</a></li>
						<li><a href="#post-3785">Hatchetfish</a></li>
						<li><a href="#post-3760">Ibis</a></li>
						<li><a href="#post-3762">Jade Plant</a></li>
						<li><a href="#post-3730">Kangaroo</a></li>
						<li><a href="#post-3718">Kudzu</a></li>
						<li><a href="#post-3774">Least Chipmunk</a></li>
						<li><a href="#post-3680">Lichen</a></li>
						<li><a href="#post-3742">Lobster "Language"</a></li>
						<li><a href="#post-3780">Loggerhead Sea Turtle</a></li>
						<li><a href="#post-3744">Loriope/Plants</a></li>
						<li><a href="#post-3736">Lupine</a></li>
						<li><a href="#post-3716">Millipede</a></li>
						<li><a href="#post-3778">Mind-bending Hairworm</a></li>
						<li><a href="#post-3766">Pacific Frog</a></li>
						<li><a href="#post-3724">Pony</a></li>
						<li><a href="#post-3752">Pygmy Marmoset</a></li>
						<li><a href="#post-3722">Rabbit</a></li>
						<li><a href="#post-3728">Robin</a></li>
						<li><a href="#post-3740">Roosevelt Elk</a></li>
						<li><a href="#post-3738">Roundworm</a></li>
						<li><a href="#post-3714">Sandpiper</a></li>
						<li><a href="#post-3770">Sloth</a></li>
						<li><a href="#post-3768">Sloth Moth</a></li>
						<li><a href="#post-3712">Sunflower</a></li>
						<li><a href="#post-3789">Sweet Cherry</a></li>
						<li><a href="#post-3776">Tulip</a></li>
						<li><a href="#post-3758">White-tailed Deer</a></li>
						<li><a href="#post-3746">Wild blueberry</a></li>
					</ul>
	

					<?php
						$i = 1;
						while ( $biome_posts->have_posts() ) : $biome_posts->the_post();
							$thumb_class = (($i % 2) == 0) ? 'biome-img-right' : 'biome-img-left';
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="page-header">
							<h3 class="page-title">
								<?php the_date( 'l, F j, Y' ); ?>:
								<?php the_title(); ?>
							</h3>
						</header><!-- .entry-header -->

						<?php if ( has_post_thumbnail() ): ?>
						<?php the_post_thumbnail( array(320, 320), array('class' => $thumb_class) ); ?>
						<?php endif; ?>
						<?php the_content(); ?>
						<a href="#top">Back to Top</a>
						<div class="clearfix"></div>
					</article><!-- #post-## -->
					<?php $i++; ?>
					<?php endwhile; // end biome post loop. ?>
					<?php wp_reset_query(); /* REQUIRED */ ?>
				</div>
			</div>
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->

<?php endif; ?>

<?php get_footer(); ?>

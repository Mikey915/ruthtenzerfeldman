<?php get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="main-content-inner col-12 col-lg-9">
                <div class="content-background">

                    <?php if ( have_posts() ) : ?>

                        <?php /* Start the Loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to overload this in a child theme then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part('content', 'page');
                            ?>

                        <?php endwhile; ?>

                        <?php _tk_content_nav( 'nav-below' ); ?>

                    <?php else : ?>

                        <?php get_template_part( 'no-results', 'index' ); ?>

                    <?php endif; ?>

                    <div class="clearfix"></div>

                </div><!--end content-background-->

                <?php if ( !is_acf_field_empty('homepage_featured_area') ): ?>
                <div class="homepage-featured-content-background">
                    <div class="entry-content">

                    <?php the_field('homepage_featured_area'); ?>

                    </div>

                    <div class="clearfix"></div>
                </div><!--end content-background-->
                <?php endif; ?>
                <?php get_sidebar(); ?>
            </div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
            <!-- <div class="shawl-wrapper">
                <div class="shawl"><a href="/time-travel">
                    <img src="/wp-content/themes/RuthSite/img/tassels.png">
                    <div class="first-brown"></div>
                    <div class="first-green"></div>
                    <div class="salmon"></div>
                    <div class="second-green"></div>
                    <div class="second-brown"><p class="rotate">Time Travel!</p></div>
                </a></div>
            </div> -->
        </div><!-- close .row -->
    </div><!-- close .container -->
<?php get_footer(); ?>

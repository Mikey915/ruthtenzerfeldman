<?php
/*
Template Name: Time Travel Page
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php
	$time_travel_page_title = 'Time Travel';
	$time_travel_page = get_page_by_title( $time_travel_page_title );

	// If this is the Time Travel root page, load one of its children at random instead.
	if ( get_the_title() == $time_travel_page_title ) {
		$rand_page_args = array(
			'post_parent' => $time_travel_page->ID,
			'post_type' => 'page',
			'posts_per_page' => 1,
			'orderby' => 'rand'
		);
		$rand_page = new WP_Query( $rand_page_args );
		if ( $rand_page->have_posts() ) $rand_page->the_post();
	}
?>

<h1 class="page-title">Time Travel!</h1>

<div class="container">
	<div class="row">
		<div class="col-12 holder">
			<div class="row">
				<div class="col-8 col-offset-2 time-travel">
					<p class="time-travel-opening">GUZZAP!  Welcome to...</p>
					<h1 class="time-travel-year"><?php the_field('time_travel_year'); ?></h1>
					<h2 class="time-travel-facts">Facts.</h2>
					<?php the_field('time_travel_facts'); ?>
					<h2 class="time-travel-fiction">Fiction.</h2>
					<?php the_field('time_travel_fiction'); ?>
					<h2 class="time-travel-extras">More.</h2>
					<?php the_field('time_travel_extra'); ?>
					<img class="dragonfly-time-travel-first" alt="" src="/wp-content/themes/RuthSite/img/dragonfly-hr.png">
					<div class="testimonials"><blockquote><p>&ldquo;People like us, who believe in physics, know that the distinction between past, present, and future, is only a stubbornly persistent illusion.&rdquo;</p><footer>&mdash;Albert Einstein</footer></blockquote></div>
					<img class="dragonfly-time-travel" alt="" src="/wp-content/themes/RuthSite/img/dragonfly-hr.png">

					<p>Interested in time travel? Start <a href="http://www.pbs.org/wgbh/nova/time/resources.html">HERE</a> for Nova's serious and not-so-serious resource guide.</p>
					<img class="dragonfly-time-travel" alt="" src="/wp-content/themes/RuthSite/img/dragonfly-hr.png">

					<p>Check back for more times and places. <a href="/contact">Contact me</a> to feature your own time travel destinations. This page is for you, too.</p>

					<?php if ( is_active_sidebar( 'sidebar-3') ): ?>
					<aside id="mailing-list-signup">
						<?php do_action( 'before_sidebar' ); ?>
						<?php dynamic_sidebar( 'sidebar-3' ); ?>
					</aside>
					<?php endif; ?>
					
				</div>
			</div><!--close .row-->
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
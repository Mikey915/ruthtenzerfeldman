<?php
/*
Template Name: Speaking and Events Page
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php
	$speaking_photo = get_field('speaking_photo');
	$event_calendar = get_field('event_calendar');
?>

<h1 class="page-title">Speaking &amp; Events</h1>

<div class="container">
	<div class="row">
		<div class="col-12 content-background-no-pad holder">
			<div class="row">
				<div class="col-12 col-sm-5 col-lg-4 action-sidebar even-height">
					<img class="action-image" src="<?php echo $speaking_photo['url']; ?>" alt="<?php echo $speaking_photo['alt']; ?>" title="<?php echo $speaking_photo['title']; ?>"/>
					<a class="book-buy-btns btn btn-default" href="/contact">Book Ruth to Speak</a>

					<?php if (strlen(trim($event_calendar)) > 0): ?>
					<div class="event-calendar">
						<?php the_field('event_calendar'); ?>
					</div>
					<?php endif; ?>
				
				</div>

				<div class="col-12 col-sm-7 col-lg-8 even-height">
					<div class="main-info">
						<?php the_content(); ?>
					</div>
			
				</div>
			</div><!--close .row-->
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->

<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>
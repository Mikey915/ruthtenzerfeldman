<?php
/*
Template Name: Book Catalog Page
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<h1 class="page-title"><?php the_title(); ?></h1>
<?php
	$page_content = get_the_content();
	$book_args = array(
		'post_parent' => get_the_ID(),
		'post_type' => 'page',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
?>

<?php endwhile; // end of the loop. ?>

<div class="container">
	<div class="row">
		<div class="main-content-inner col-12">
			<div class="col-12 col-lg-3 visible-lg">
				<nav class="subpage-nav">
					<ul class="non-semantic-protector">
						<li class="active"><a href="#">All Books</a></li>
						<?php foreach (get_all_book_series() as $key => $value): ?>
						<li><a data-book-series="<?php echo $key; ?>" href="#"><?php echo $value; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</nav>
			</div><!--end col-lg-3-->
			<div class="col-12 col-lg-9">
				<div class="content-background-half-pad">
					<?php the_content(); ?>
					<ul id="book-list">
					<?php
						$book_query = new WP_Query( $book_args );
						while ( $book_query->have_posts() ): $book_query->the_post();
							$book_cover = get_field('book_cover');
					?>
						<li class="<?php the_field('book_series'); ?>">
							<a href="<?php the_permalink(); ?>">
							<img class="catalog-image" src="<?php echo $book_cover['url']; ?>" alt="<?php echo $book_cover['alt']; ?>" title="<?php echo $book_cover['title']; ?>"/>
							<h3><?php the_title(); ?></h3></a>
							<div class="book-short-description"><?php the_field('short_description'); ?></div>
							<div class="book-long-description"><?php the_field('long_description'); ?></div>
							<a class="learn-more-link" href="<?php the_permalink(); ?>">Learn more <span class="glyphicon glyphicon-chevron-right"></span><span class="glyphicon glyphicon-chevron-right"></span></a>
							<a href="<?php the_permalink(); ?>" class="btn btn-default learn-more-button">Learn more</a>
							<!-- <?php /* book_buy_button_dropdown(); */ ?> -->
						</li>
					<?php endwhile; // end of the book loop. ?>
					<?php wp_reset_query(); /* REQUIRED */ ?>
					</ul>
					<div class="clearfix"></div>
				</div>
			</div>
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->
<script type="text/javascript">
jQuery(document).ready(function ($) {
	'use strict';

	function fixColumns() {
		$('ul#book-list > li').css('clear', 'none');
		$('ul#book-list > li:visible:odd').css('clear', 'both');
	}

	$('ul#book-list > li').first().addClass('first-book');

	$('nav.subpage-nav ul li a').first().click(function () {
		var books, subnav;

		books = $('ul#book-list > li');
		subnav = $('nav.subpage-nav ul li');

		subnav.removeClass('active');
		$(this).parent().addClass('active');

		books.removeClass('first-book').show();
		books.first().addClass('first-book');
		fixColumns();
	});

	$('nav.subpage-nav ul li a').slice(1).click(function () {
		var books, series, subnav;

		books = $('ul#book-list > li');
		series = books.filter('.' + $(this).attr('data-book-series'));
		subnav = $('nav.subpage-nav ul li');

		subnav.removeClass('active');
		$(this).parent().addClass('active');

		books.removeClass('first-book').hide();
		series.show();
		series.filter(':visible').first().addClass('first-book');
		fixColumns();
	});

	fixColumns();
});
</script>


<?php get_footer(); ?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */

get_header(); ?>

<h1 class="page-title"><?php the_title(); ?></h1>

<div class="container">
	<div class="row">
		<div class="col-12 content-background-no-pad holder">
			<div class="row">
				<div class="col-12 col-sm-5 col-lg-4 action-sidebar even-height">
					<?php if ( is_active_sidebar( 'sidebar-3') ): ?>
					<aside id="mailing-list-signup">
						<?php do_action( 'before_sidebar' ); ?>
						<?php dynamic_sidebar( 'sidebar-3' ); ?>
					</aside>
					<?php endif; ?>
				</div>
				<div class="col-12 col-sm-7 col-lg-8 even-height">
					<div class="main-info no-title">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
						<?php endwhile; // end of the loop. ?>
					</div>

				</div>
			</div><!--close .row-->
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->
<?php get_footer(); ?>

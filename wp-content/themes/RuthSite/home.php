<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="main-content-inner col-12 col-lg-9">
				<div class="content-background">

	<?php if ( have_posts() ) : ?>

        <?php if ( get_query_var('paged') <= 1 ): ?>
        <header class="blog-title">
            <h3>Ruth's Blog: The Interlace Place</h3>
            <img alt="" src="/wp-content/themes/RuthSite/img/dragonfly-hr.png">
        </header>
        <?php endif; ?>

		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php
				/* Include the Post-Format-specific template for the content.
				 * If you want to overload this in a child theme then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );
			?>
			<img alt="" src="/wp-content/themes/RuthSite/img/dragonfly-hr.png">

		<?php endwhile; ?>

		<?php _tk_content_nav( 'nav-below' ); ?>

	<?php else : ?>

		<?php get_template_part( 'no-results', 'index' ); ?>

	<?php endif; ?>
				</div><!--end content-background-->
<?php get_sidebar(); ?>
		</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
	</div><!-- close .row -->
</div><!-- close .container -->
<?php get_footer(); ?>

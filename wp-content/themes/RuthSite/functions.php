<?php
/**
 * _tk functions and definitions
 *
 * @package _tk
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */



/******* BEGIN RUTHSITE THEME SPECIFIC FUNCTIONS *******/
function custom_excerpt_length( $length ) {
	return 150;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function _ruth_homepage_sidebar_box_init() {
	register_sidebar( array(
		'name'          => __( 'Homepage Sidebar Box', '_tk' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', '_ruth_homepage_sidebar_box_init' );

function _ruth_mailchimp_box_init() {
	register_sidebar( array(
		'name'          => __( 'MailChimp Box', '_tk' ),
		'id'            => 'sidebar-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', '_ruth_mailchimp_box_init' );

if ( ! function_exists( 'get_breadcrumb' ) ) :
function get_breadcrumb() {
	$buffer = '';
	$buffer .= '<ol class="breadcrumb visible-lg">';
	if (!is_home()) {
		$buffer .= '<li><a href="';
		$buffer .= get_option('home');
		$buffer .= '">';
		$buffer .= 'Home';
		$buffer .= '</a></li>';
		if (is_category() || is_single()) {
			$buffer .= '<li>' . get_the_category() . '</li>';
			if (is_single()) {
				$buffer .= '<li>' . get_the_title() . '</li>';
			}
		} elseif (is_page()) {
			global $post;
			if ($post->post_parent) {
				$anc = get_post_ancestors( $post->ID );

				foreach ( $anc as $ancestor ) {
					$output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> ';
				}
				$buffer .= $output;
				$buffer .= '<li class="active">' . get_the_title() . '</li>';
			} else {
				$buffer .= '<li class="active">' . get_the_title() . '</li>';
			}
		}
	}
	elseif (is_tag()) {single_tag_title();}
	elseif (is_day()) {$buffer .= '<li class="active">Archive for '; the_time('F jS, Y'); $buffer .= '</li>';}
	elseif (is_month()) {$buffer .= '<li class="active">Archive for '; the_time('F, Y'); $buffer .= '</li>';}
	elseif (is_year()) {$buffer .= '<li class="active">Archive for '; the_time('Y'); $buffer .= '</li>';}
	elseif (is_author()) {$buffer .= '<li class="active">Author Archive'; $buffer .= '</li>';}
	elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {$buffer .= '<li class="active">Blog Archives'; $buffer .= '</li>';}
	elseif (is_search()) {$buffer .= '<li class="active">Search Results'; $buffer .= '</li>';}
	$buffer .= '</ol>';
	return $buffer;
}
endif; // get_breadcrumb

if ( ! function_exists( 'the_breadcrumb' ) ) :
function the_breadcrumb() {
	echo get_breadcrumb();
}
endif; // the_breadcrumb

if ( ! function_exists( 'get_all_book_series' ) ) :
function get_all_book_series() {
	// Field key for "Book Series" field. Apparently this is the preferred
	// method for requesting fields, despite how incredibly stupid it is. It is
	// also *required* in order to avoid having to query for the choices from
	// within the loop.
	$field_key = 'field_523d06fa61eb1';

	$field = get_field_object($field_key);
	return $field['choices'];
}
endif; // get_all_book_series

if ( ! function_exists( 'get_latest_book' ) ) :
function get_latest_book() {
	$catalog_page = get_page_by_title( 'Books' );
	$latest_book_uri = get_field('latest_book', $catalog_page->ID);
	$latest_book_slug = preg_replace(
		'/^[A-Za-z]*:\/\/[^\/]+\/?/', '', // Remove scheme://domain/
		preg_replace('/\/$/', '', $latest_book_uri) // Remove trailing slash
	);

	$latest_book_args = array(
		'pagename' => $latest_book_slug,
		'post_type' => 'page',

		// These should be unnecessary, but better safe than sorry.
		'posts_per_page' => 1,
		'orderby' => 'date',
		'order' => 'ASC'
	);
	$query = new WP_Query($latest_book_args);
	return $query;
}
endif; // get_latest_book

if ( ! function_exists( 'get_buy_link_field_keys' ) ) :
function get_buy_link_field_keys() {
	// Return the buy link fields in the order they should appear.
	// Doing it this way because (mutable) arrays can't be constants.
	return array(
		'field_523d1bd854048', // barnes_and_noble_link
		'field_523d1c3bcbcb5', // powells_link
		'field_523d196f79952', // indiebound_link
		'field_523d1c6acbcb6', // amazon_link
	);
}
endif; // get_buy_link_field_keys

if ( ! function_exists( 'book_buy_button_dropdown' ) ) :
function book_buy_button_dropdown() {
	global $post;

	$dropdown_wrapper = <<<DDWRAPPER
	<div class="btn-group buy-button">
		<button type="button" class="btn btn-default" data-toggle="dropdown">
			Buy the book
		</button>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu">%s</ul>
	</div>
DDWRAPPER;

	// Generate <li> elements for each choice in the dropdown.
	$links = array();
	foreach (get_buy_link_field_keys() as $field) {
		$field_object = get_field_object($field, $post->ID, true);
		$field_value = get_field($field, $post->ID);
		$uri = trim($field_value);
		if (!empty($uri)) {
			$links[] = sprintf('<li><a href="%s">%s</a></li>',
				$uri,
				$field_object['label']
			);
		}
	}

	if ( count( $links ) == 0 )
		return; // ABORT!!! ABORT!!!

	// Add in the <li> dividers between each choice, wrap everything, and print.
	printf($dropdown_wrapper, implode('<li class="divider"></li>', $links));
}
endif; // book_buy_button_dropdown

if ( ! function_exists( 'book_buy_buttons' ) ) :
function book_buy_buttons() {
	global $post;

	// Output <a> elements for each available option.
	foreach (get_buy_link_field_keys() as $field) {
		$field_object = get_field_object($field, $post->ID, true);
		$field_value = get_field($field, $post->ID);
		$uri = trim($field_value);
		if (!empty($uri)) {
			printf('<a class="book-buy-btns btn btn-default" href="%s">%s</a>',
				$uri,
				$field_object['label']
			);
		}
	}
}
endif; // book_buy_buttons

if ( ! function_exists( 'is_acf_field_empty' ) ) :
function is_acf_field_empty($field_name) {
    global $post;

    $raw_content = get_field($field_name);
    if (strlen(trim(html_entity_decode(strip_tags($raw_content)))) == 0) {
        return true;
    }

    return false;
}
endif; // is_acf_field_empty

if ( ! function_exists( 'is_the_content_empty' ) ) :
function is_the_content_empty() {
    global $post;

    $raw_content = $post->post_content;
    if (strlen(trim(html_entity_decode(strip_tags($raw_content)))) == 0) {
        return true;
    }

    return false;
}
endif; // is_the_content_empty
/******* END RUTHSITE THEME SPECIFIC FUNCTIONS *******/



if ( ! function_exists( '_tk_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function _tk_setup() {
	global $cap, $content_width;

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	if ( function_exists( 'add_theme_support' ) ) {

		/**
		 * Add default posts and comments RSS feed links to head
		*/
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support( 'post-thumbnails' );

		/**
		 * Enable support for Post Formats
		*/
		add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

		/**
		 * Setup the WordPress core custom background feature.
		*/
		add_theme_support( 'custom-background', apply_filters( '_tk_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

	}

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on _tk, use a find and replace
	 * to change '_tk' to the name of your theme in all the template files
	*/
	load_theme_textdomain( '_tk', get_template_directory() . '/languages' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	*/
	register_nav_menus( array(
		'primary'  => __( 'Header bottom menu', '_tk' ),
	) );

}
endif; // _tk_setup
add_action( 'after_setup_theme', '_tk_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function _tk_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', '_tk' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', '_tk_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function _tk_scripts() {
	wp_enqueue_style( '_tk-style', get_stylesheet_uri() );

	// load bootstrap css
	wp_enqueue_style( '_tk-bootstrap', get_template_directory_uri() . '/includes/resources/bootstrap/css/bootstrap.min.css' );

	// load bootstrap js
	wp_enqueue_script('_tk-bootstrapjs', get_template_directory_uri().'/includes/resources/bootstrap/js/bootstrap.min.js', array('jquery') );

	// load the glyphicons
	wp_enqueue_style( '_tk-glyphicons', get_template_directory_uri() . '/includes/resources/glyphicons/css/bootstrap-glyphicons.css' );

	// load bootstrap wp js
	wp_enqueue_script( '_tk-bootstrapwp', get_template_directory_uri() . '/includes/js/bootstrap-wp.js', array('jquery') );

	wp_enqueue_script( '_tk-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( '_tk-keyboard-image-navigation', get_template_directory_uri() . '/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}
}
add_action( 'wp_enqueue_scripts', '_tk_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/jetpack.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/bootstrap-wp-navwalker.php';

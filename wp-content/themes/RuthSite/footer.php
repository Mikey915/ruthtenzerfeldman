<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>

</div><!-- close .main-content -->

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="site-footer-inner col-12">
			
				<div class="site-info">
					<p>&copy; <?php echo date('Y'); ?> Ruth Tenzer Feldman
					<span class="sep"> | </span>
					Site by <a href="http://www.kateburkett.com">Kate Burkett</a></p>
				</div><!-- close .site-info -->
			
			</div>	
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->

<?php wp_footer(); ?>
</div><!--end wrapper-->
</body>
</html>